/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.w4rlock.imagelivewallpaper;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String PACKAGE_NAME = "com.w4rlock.imagelivewallpaper";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
}
