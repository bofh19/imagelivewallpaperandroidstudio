package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity;

public class Configs {
	public static final String STATE_SELECTED_NAVIGATION_ITEM = "com.w4rlock.imagelivewallpaper.browse_images_activity.selected_navigation_item";

    public final static String FILE_BROWSE_TAG_FRAGMENT = "com.w4rlock.imagelivewallpaper.browse_images_activity.file_browse_TAG_FRAGMENT";
    public final static String IMAGE_LIST_TAG_FRAGMENT = "com.w4rlock.imagelivewallpaper.browse_images_activity.image_list_TAG_FRAGMENT";
}
