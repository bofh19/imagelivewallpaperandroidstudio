package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.ImagesListView;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.imagelivewallpaper.R;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.provider.MyDataSource;

/**
 * Created by skarumuru on 10/4/13.
 */
public class ImagesListViewMainFragment extends Fragment {
    private ViewGroup rootView;
    private Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = (ViewGroup) inflater.inflate(R.layout.images_list_view_main_layout, container, false);
        context = this.getActivity().getApplicationContext();
        startCleanUp();
        RelativeLayout ll = (RelativeLayout) rootView.findViewById(R.id.images_list_view_main_ll_layout);

        ll.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                Log.d(AllConfigs.DEBUG_TAG, " ----- TOUCHED EVENT --- ");
                onTouchEvent(motionEvent);
                return true;
            }
        });
        return rootView;
    }

    private void startCleanUp() {
        CleanUpOnRequest cleanUpOnRequest = new CleanUpOnRequest(this.getActivity());
        cleanUpOnRequest.execute();
    }

    public Fragment getFragment(int position) {
        Fragment fragment = new ImagesListViewEachFragment();
        Bundle args = new Bundle();
        args.putInt(ImagesListViewEachFragment.ARG_SECTION_NUMBER, position + 1);
        fragment.setArguments(args);
        return fragment;
    }

    private int position = 0;

    private void setAdapter(int given_position) {
        Fragment fg = getFragment(given_position);
        FragmentTransaction ft = getFragmentManager().beginTransaction();

        ft.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right);
        ft.replace(R.id.images_list_view_fragment, fg, "fragment");
// Start the animated transition.
        ft.commit();
        setListeners();

    }


    private void setListeners() {
        TextView tPrev = (TextView) rootView.findViewById(R.id.images_list_view_main_layout_text_prev);
        TextView tNext = (TextView) rootView.findViewById(R.id.images_list_view_main_layout_text_next);
        tPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = position - 1;
                if (position < 0) {
                    position = 0;
                    return;
                }
                setAdapter(position);
                System.gc();
            }
        });
        tNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                position = position + 1;
                if (position >= 4) {
                    position = 3;
                    return;
                }
                setAdapter(position);
                System.gc();
            }
        });
    }

    class CleanUpOnRequest extends AsyncTask<Object, Object, Object> {

        private ProgressDialog dialog;
        private int total_counter;

        public CleanUpOnRequest(Activity activity) {
            dialog = new ProgressDialog(activity);
            total_counter = 0;
        }

        @Override
        protected void onPreExecute() {
            this.dialog.setMessage("Cleaning up images that do not exist please wait");
            this.dialog.setTitle("Please Wait");
            this.dialog.setCancelable(false);
            this.dialog.setCanceledOnTouchOutside(false);
            if (!this.dialog.isShowing())
                this.dialog.show();
        }

        @Override
        protected Object doInBackground(Object... objects) {
            MyDataSource myDataSource = new MyDataSource();
//            ArrayList<EnabledPath> allEnabledPaths = myDataSource.getAllValues(context);
//            for(EnabledPath currentEnabledPath:allEnabledPaths){
//                boolean check = new File(currentEnabledPath.getPath()).exists();
//                Log.d(AllConfigs.DEBUG_TAG,"checking file "+currentEnabledPath.getPath()+" status "+check);
//                if(!check){
//                    boolean status = myDataSource.removeCurrentPath(context,currentEnabledPath);
//                    Log.d(AllConfigs.DEBUG_TAG,"status on "+currentEnabledPath.getPath()+" "+status);
//                    total_counter = total_counter + 1;
//                }
//            }
            myDataSource.cleanUp(context);
            return true;
        }

        @Override
        protected void onPostExecute(Object result) {
            Boolean status = (Boolean) result;
            if (dialog.isShowing())
                dialog.dismiss();
            Toast.makeText(context, "Removed " + total_counter + " links to images that doesn't exist anymore ", Toast.LENGTH_SHORT);
            setAdapter(0);
        }
    }

    private static final int MIN_DISTANCE_SHORT = 25;
    private float downX, downY, upX, upY;

    //    DisplayMetrics dm = getResources().getDisplayMetrics();
    int REL_SWIPE_MIN_DISTANCE_SHORT = (int) (MIN_DISTANCE_SHORT * 1 / 160.0f);

    private int MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;


    public void onTouchEvent(MotionEvent event) {
        Log.d("backpaper2-wallpaper-server-touch", "x: " + event.getX() + " y: " + event.getY());
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return;
            }
            case MotionEvent.ACTION_UP: {

                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
//                float deltaY = downY - upY;

                // swipe horizontal?
                if (Math.abs(deltaX) > MIN_DISTANCE) {
                    // left or right
                    if (deltaX < 0) {
                        onLeftToRightSwipe();
//	                	return;
                    }
                    if (deltaX > 0) {
                        onRightToLeftSwipe();
//	                	return;
                    }
                } else {
                    Log.i(AllConfigs.DEBUG_TAG, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
//	                    return; // We don't consume the event
                }

//                // swipe vertical?
//                if (onBottomTop) {
//                    MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_LONG;
//                } else {
//                    MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;
//                }
//                if (Math.abs(deltaY) > MIN_DISTANCE) {
//                    // top or down
//                    if (deltaY < 0) {
//                        onTopToBottomSwipe(deltaY);
//                        return;
//                    }
//                    if (deltaY > 0) {
//                        onBottomToTopSwipe(deltaY);
//                        return;
//                    }
//                } else {
//                    Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
//                    return; // We don't consume the event
//                }

                return;
            }

        }
    }

    private void onRightToLeftSwipe() {
        Log.d(AllConfigs.DEBUG_TAG, "=---------------------->----- on right to left swipe");
    }

    private void onLeftToRightSwipe() {
        Log.d(AllConfigs.DEBUG_TAG, "=------------------------------<-- on right to left swipe");
    }
}

