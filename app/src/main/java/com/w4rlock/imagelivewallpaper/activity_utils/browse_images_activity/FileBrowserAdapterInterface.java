package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity;

/**
 * Created by saikrishna on 11/18/13.
 */
public interface FileBrowserAdapterInterface {
    public boolean addPathToDB(String path,Boolean status,int listNum);
}
