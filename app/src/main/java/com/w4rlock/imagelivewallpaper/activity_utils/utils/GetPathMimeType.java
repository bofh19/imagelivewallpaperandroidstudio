package com.w4rlock.imagelivewallpaper.activity_utils.utils;

import android.webkit.MimeTypeMap;

/**
 * Created by saikrishna on 11/19/13.
 */
public class GetPathMimeType {
    public static String getMimeType(String url) {
        final String[] audioExt = {"mp3", "mp4", "ogg", "wav", "wmv"};
        final String[] imageExt = {"jpg", "jpeg", "png", "gif"};
        final String[] docExt = {"pdf", "doc", "docx", "xls", "txt"};
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            MimeTypeMap mime = MimeTypeMap.getSingleton();
            type = mime.getMimeTypeFromExtension(extension);
        }else{
            return "null/null";
        }
        if (type == null) {
            try {
                String exten = url.substring(url.lastIndexOf('.') + 1);
                for (String str : audioExt) {
                    if (exten.toLowerCase().contains(str)) {
                        return "audio/" + exten;
                    }
                }
                for (String str : imageExt) {
                    if (exten.toLowerCase().contains(str)) {
                        return "image/" + exten;
                    }
                }
                for (String str : docExt) {
                    if (exten.toLowerCase().contains(str)) {
                        return "text/" + exten;
                    }
                }
            } catch (Exception e) {
                return "null/null";
            }
        }else{
            return type;
        }
        return "null/null";
    }
}
