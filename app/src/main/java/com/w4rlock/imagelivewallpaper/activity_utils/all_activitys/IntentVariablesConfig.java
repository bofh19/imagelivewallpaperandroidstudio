package com.w4rlock.imagelivewallpaper.activity_utils.all_activitys;

/**
 * Created by skarumuru on 10/9/13.
 */
public class IntentVariablesConfig {
    //public static final String refreshFilesListSettings = "com.w4rlock.imagelivewallpaper.imageslist.refresh.settings";
    //public static final String refreshFilesListVariable = "com.w4rlock.imagelivewallpaper.imageslist.refresh.variable";

    public static final String imageLiveWallpaperListSettings = "com.w4rlock.imagelivewallpaper.imageslist.settings";
    public static final String imageLiveWallpaperListSelectedVariable = "com.w4rlock.imagelivewallpaper.imageslist.selected.variable";
    public static final String imageLiveWallpaperEnabledListsVariable = "com.w4rlock.imagelivewallpaper.imageslist.enabled.variable";
}
