package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity;

/**
 * Created by skarumuru on 10/3/13.
 */

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.w4rlock.imagelivewallpaper.R;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.models.FileModel;
import com.w4rlock.imagelivewallpaper.provider.MyDataSource;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class FileListDisplayAdapter extends ArrayAdapter<FileModel> {
    Context context;
    int layoutResourceId;
    ArrayList<FileModel> fileObjects;

    public ImageLoader imageLoader;
    public DisplayImageOptions doptions;
    public static final String DEBUG_TAG = AllConfigs.DEBUG_TAG;
    private FileBrowserAdapterInterface fileBrowserAdapterInterface;
    public FileListDisplayAdapter(Context context, int layoutResourceId, ArrayList<FileModel> fileObjects,FileBrowserAdapterInterface fileBrowserAdapterInterface) {
        super(context, layoutResourceId, fileObjects);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.fileObjects = fileObjects;
        this.fileBrowserAdapterInterface =  fileBrowserAdapterInterface;


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        doptions = new DisplayImageOptions.Builder()
                .cacheOnDisc()
                .showStubImage(R.drawable.ic_stub)
                .showImageForEmptyUri(R.drawable.ic_empty)
                .showImageOnFail(R.drawable.ic_error)
                .cacheInMemory()
                .cacheOnDisc()
                .displayer(new RoundedBitmapDisplayer(10))
                .build();
    }

    @Override
    public View getView(final int position,View convertView,ViewGroup parent){
        View row = convertView;
        FileViewHolder holder;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layoutResourceId,parent,false);
            holder = new FileViewHolder();
            holder.icon = (ImageView)row.findViewById(R.id.icon);
            holder.fnameTxt = (TextView)row.findViewById(R.id.f_name_main);
            holder.fdescTxt = (TextView)row.findViewById(R.id.f_name_desc);
            holder.checkBox1 = (CheckBox)row.findViewById(R.id.f_row_checkBox_1);
            holder.checkBox2 = (CheckBox)row.findViewById(R.id.f_row_checkBox_2);
            holder.checkBox3 = (CheckBox)row.findViewById(R.id.f_row_checkBox_3);
            holder.checkBox4 = (CheckBox)row.findViewById(R.id.f_row_checkBox_4);
            row.setTag(holder);
        }else{
            holder = (FileViewHolder)row.getTag();
        }
        FileModel currentFile = fileObjects.get(position);
       // Log.d(DEBUG_TAG,"inside adapter "+currentFile.getName());
        holder.fnameTxt.setText(currentFile.getName());
        holder.fdescTxt.setText(currentFile.getDesc());
        //holder.checkBox.setChecked(currentFile.getCheckBoxStatus());
        if(currentFile.getCheckPossible()){
           // holder.checkBox1.setChecked(userSelectionDataSource.getStatus(currentFile.getAbsPath(),1));
            //holder.checkBox2.setChecked(userSelectionDataSource.getStatus(currentFile.getAbsPath(),2));
            //holder.checkBox3.setChecked(userSelectionDataSource.getStatus(currentFile.getAbsPath(),3));
            //holder.checkBox4.setChecked(userSelectionDataSource.getStatus(currentFile.getAbsPath(),4));
            holder.checkBox1.setChecked(false);
            holder.checkBox2.setChecked(false);
            holder.checkBox3.setChecked(false);
            holder.checkBox4.setChecked(false);
            CheckBoxStatusGetter checkBoxStatusGetter1 = new CheckBoxStatusGetter(holder.checkBox1);
            checkBoxStatusGetter1.execute(currentFile.getAbsPath(),1);

            CheckBoxStatusGetter checkBoxStatusGetter2 = new CheckBoxStatusGetter(holder.checkBox2);
            checkBoxStatusGetter2.execute(currentFile.getAbsPath(),2);

            CheckBoxStatusGetter checkBoxStatusGetter3 = new CheckBoxStatusGetter(holder.checkBox3);
            checkBoxStatusGetter3.execute(currentFile.getAbsPath(),3);

            CheckBoxStatusGetter checkBoxStatusGetter4 = new CheckBoxStatusGetter(holder.checkBox4);
            checkBoxStatusGetter4.execute(currentFile.getAbsPath(),4);
//            setCheckedStatus(holder.checkBox1,currentFile.getAbsPath(),1);
//            setCheckedStatus(holder.checkBox2,currentFile.getAbsPath(),2);
//            setCheckedStatus(holder.checkBox3,currentFile.getAbsPath(),3);
//            setCheckedStatus(holder.checkBox4,currentFile.getAbsPath(),4);
            holder.checkBox1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToDb(1,view,position);
                }
            });
            holder.checkBox2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToDb(2,view,position);
                }
            });
            holder.checkBox3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToDb(3,view,position);
                }
            });
            holder.checkBox4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    addToDb(4,view,position);
                }
            });
        }
        if(!currentFile.getCheckPossible()){
            LinearLayout lCheckBoxLayout = (LinearLayout) holder.checkBox1.getParent();
            lCheckBoxLayout.setVisibility(View.GONE);
        }else{
            LinearLayout lCheckBoxLayout = (LinearLayout) holder.checkBox1.getParent();
            lCheckBoxLayout.setVisibility(View.VISIBLE);
        }
        try{
            String type = currentFile.getMimeType();
            if(type.contains("image")){
                String location = "file://"+currentFile.getAbsPath();
                Log.d(DEBUG_TAG,"image type found icon : "+location);
                imageLoader.displayImage(location,holder.icon);
            }else if(type.contains("text")){
                imageLoader.displayImage("drawable://"+R.drawable.notes_icon,holder.icon);
            }else if(type.contains("audio")){
                imageLoader.displayImage("drawable://"+R.drawable.itunes_icon,holder.icon);
            }else if(type.contains("application")){
                imageLoader.displayImage("drawable://"+R.drawable.folder_apps_icon,holder.icon);
            }else{
                imageLoader.displayImage("drawable://"+R.drawable.ic_stub,holder.icon);
            }
        }catch(Exception e){
            //Log.d(DEBUG_TAG,"no type");
            imageLoader.displayImage("drawable://"+R.drawable.ic_stub,holder.icon);
        }
        if(currentFile.getFolder()){
            imageLoader.displayImage("drawable://"+R.drawable.folder_icon_1,holder.icon);
        }
        Animation animation = AnimationUtils.loadAnimation(context.getApplicationContext(), android.R.anim.fade_in);
        row.startAnimation(animation);
        return row;
    }

    private void addToDb(int checkBoxNum,View view,int position) {
        Log.d(DEBUG_TAG,"check box event raised");
        LinearLayout lv = (LinearLayout)view.getParent();
        CheckBox cv;
        if(checkBoxNum == 1)
            cv = (CheckBox)lv.findViewById(R.id.f_row_checkBox_1);
        else if (checkBoxNum == 2)
            cv = (CheckBox)lv.findViewById(R.id.f_row_checkBox_2);
        else if (checkBoxNum == 3)
            cv = (CheckBox)lv.findViewById(R.id.f_row_checkBox_3);
        else if (checkBoxNum == 4)
            cv = (CheckBox)lv.findViewById(R.id.f_row_checkBox_4);
        else cv = (CheckBox)lv.findViewById(R.id.f_row_checkBox_1);

        RelativeLayout rv = (RelativeLayout)lv.getParent().getParent();
        TextView tv = (TextView)rv.findViewById(R.id.f_name_main);
        Boolean b = cv.isChecked();
        Log.d(DEBUG_TAG,"State Changed to "+b+" "+tv.getText());
        //Toast.makeText(context," pos "+position+fileObjects.get(position).getName(),Toast.LENGTH_SHORT).show();
        if(fileBrowserAdapterInterface.addPathToDB(fileObjects.get(position).getAbsPath(),b,checkBoxNum)){
//            if(b)
//                Toast.makeText(context,"Added Folder "+fileObjects.get(position).getName()+" Sucessfully to Rotation on List "+checkBoxNum,Toast.LENGTH_SHORT).show();
//            else
//                Toast.makeText(context,"Removed Folder "+fileObjects.get(position).getName()+" Sucessfully From Rotation on List "+checkBoxNum,Toast.LENGTH_SHORT).show();
        }
        else
            Toast.makeText(context,"Failed to Added/Remove Folder/Image "+fileObjects.get(position).getName()+" to Rotation "+AllConfigs.TRAIL_FAILED_MESSAGE,Toast.LENGTH_SHORT).show();
    }

    class CheckBoxStatusGetter extends AsyncTask<Object,Object,Object>{
        private WeakReference<CheckBox> checkBoxWeakReference;
        public CheckBoxStatusGetter(CheckBox checkBox){
            checkBoxWeakReference= new WeakReference<CheckBox>(checkBox);
        }
        @Override
        protected Object doInBackground(Object... objects) {
            String absPath = (String)objects[0];
            Integer checkBoxNum = (Integer)objects[1];
            try {
                MyDataSource tempMyDataSource = new MyDataSource();
                final Boolean status = tempMyDataSource.getStatus(context,absPath, checkBoxNum);
                return status;
            }catch (Exception e){
                Log.d(DEBUG_TAG,"Error while trying to open from adapter async task "+e);
                return false;
            }
        }

        @Override
        protected void onPostExecute(Object result){

            Boolean status = (Boolean)result;
            if(checkBoxWeakReference != null)
                try {
                    checkBoxWeakReference.get().setChecked(status);
                }catch (Exception e){

                }
        }
    }

    static class FileViewHolder{
        ImageView icon;
        TextView fnameTxt;
        TextView fdescTxt;
        CheckBox checkBox1;
        CheckBox checkBox2;
        CheckBox checkBox3;
        CheckBox checkBox4;
    }
}

