package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.ImagesListView;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.w4rlock.imagelivewallpaper.R;

/**
 * Created by skarumuru on 10/4/13.
 */
public class ImagesListViewMainFragmentAdapter extends FragmentPagerAdapter {
    private Context context;

    public ImagesListViewMainFragmentAdapter(FragmentManager fm) {
        super(fm);
    }

    public ImagesListViewMainFragmentAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment = new ImagesListViewEachFragment();
        Bundle args = new Bundle();
        args.putInt(ImagesListViewEachFragment.ARG_SECTION_NUMBER, position + 1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position){
        switch (position) {
            case 0:
                return context.getResources().getString(R.string.list_1_title).toUpperCase();
            case 1:
                return context.getResources().getString(R.string.list_2_title).toUpperCase();
            case 2:
                return context.getResources().getString(R.string.list_3_title).toUpperCase();
            case 3:
                return context.getResources().getString(R.string.list_4_title).toUpperCase();
        }
        return null;
    }
}
