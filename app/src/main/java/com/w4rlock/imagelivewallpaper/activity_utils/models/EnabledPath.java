package com.w4rlock.imagelivewallpaper.activity_utils.models;

import java.io.Serializable;

/**
 * Created by saikrishna on 11/4/13.
 */
public class EnabledPath implements Serializable {
    private String path;
    private int listNo;
    private Boolean enabledStatus;
    private String cropImgPath;
    public EnabledPath(){

    }

    public EnabledPath(String givenPath,int givenListNo){
        this.path = givenPath;
        this.listNo = givenListNo;
    }

    public EnabledPath(String givenPath,int givenListNo,Boolean givenStatus){
        this.path = givenPath;
        this.listNo = givenListNo;
        this.enabledStatus = givenStatus;
    }

    public EnabledPath(String path, int listNo, Boolean enabledStatus, String cropImgPath) {
        this.path = path;
        this.listNo = listNo;
        this.enabledStatus = enabledStatus;
        this.cropImgPath = cropImgPath;
    }

    public String getCropImgPath() {
        return cropImgPath;
    }

    public void setCropImgPath(String cropImgPath) {
        this.cropImgPath = cropImgPath;
    }

    public Boolean getEnabledStatus() {
        return enabledStatus;
    }

    public void setEnabledStatus(Boolean enabledStatus) {
        this.enabledStatus = enabledStatus;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getListNo() {
        return listNo;
    }

    public void setListNo(int listNo) {
        this.listNo = listNo;
    }
}
