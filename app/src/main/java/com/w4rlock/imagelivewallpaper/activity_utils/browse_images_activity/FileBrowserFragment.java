package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.w4rlock.imagelivewallpaper.R;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.models.EnabledPath;
import com.w4rlock.imagelivewallpaper.activity_utils.models.FileModel;
import com.w4rlock.imagelivewallpaper.activity_utils.utils.GetPathMimeType;
import com.w4rlock.imagelivewallpaper.provider.DbHelper;
import com.w4rlock.imagelivewallpaper.provider.MyDataSource;
import com.w4rlock.imagelivewallpaper.provider.tables.ImagesListTable;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.DirectoryFileFilter;
import org.apache.commons.io.filefilter.RegexFileFilter;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

/**
 * Created by skarumuru on 10/3/13.
 */
public class FileBrowserFragment extends Fragment implements FileBrowserAdapterInterface {

    public static final String DEBUG_TAG = AllConfigs.DEBUG_TAG;
    private ArrayList<FileModel> fileObjects = new ArrayList<FileModel>();
    private FileListDisplayAdapter fileListDisplayAdapter;
    private ListView filesListView;
    private TextView currentPath;
    private ViewGroup rootView = null;
    //private UserSelectionDataSource userSelectionDataSource;
    private Context context;
    private BrowseImagesInterface browseImagesInterface;
    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        try {
            this.browseImagesInterface = (BrowseImagesInterface)activity;
        }catch (ClassCastException e){
            throw new ClassCastException(activity.toString()+" must implement image change button listner");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rootView = (ViewGroup) inflater.inflate(
                R.layout.file_browser_main_view, container, false);
        context = this.getActivity().getApplicationContext();
        fileListDisplayAdapter = new FileListDisplayAdapter(this.getActivity().getApplicationContext(), R.layout.file_browser_row_view, fileObjects,this);
        filesListView = (ListView) rootView.findViewById(R.id.files_list);
        filesListView.setAdapter(fileListDisplayAdapter);
        currentPath = (TextView) rootView.findViewById(R.id.current_file_path);
        //  userSelectionDataSource = new UserSelectionDataSource(context);
        // variables init done here //
        initialPath();
        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                try {
                    FileModel cFileModel = (FileModel) adapterView.getAdapter().getItem(i);
                    if (cFileModel.getFolder()) {
                        Log.d(DEBUG_TAG, "abs path " + cFileModel.getAbsPath());
                        startFileDisplayActivity(cFileModel.getAbsPath());
                    } else if (cFileModel.getMimeType().toLowerCase().contains("image")) {
                        Intent image_intent = new Intent();
                        image_intent.setAction(Intent.ACTION_VIEW);
                        image_intent.setDataAndType(Uri.fromFile(new File(cFileModel.getAbsPath())), "image/*");
                        startActivity(image_intent);
                    } else {
                        Toast.makeText(context, "Not a Folder or Image", Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception e) {
                    Log.d(DEBUG_TAG, "exception on item click listener 1 " + e.toString());
                }
            }
        });
        return rootView;
    }

    private Boolean onInitialPath = false;

    public Boolean allowBackPressed() {
        if (onInitialPath){
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return false;
        }
        try {
            FileModel backFile = fileListDisplayAdapter.getItem(0);
            Log.d(DEBUG_TAG, "back clicked displaying path " + backFile.getAbsPath());
            if (backFile.getName().equals("External SD Card")) {
                return true;
            }
            startFileDisplayActivity(backFile.getAbsPath());
            return true;
        } catch (Exception e) {
            getActivity().getSupportFragmentManager().beginTransaction().remove(this).commit();
            return false;
        }
    }

    private void startFileDisplayActivity(String path) {
        onInitialPath = false;
        if (path == null) {
            return;
        }
        fileObjects.clear();
        if (path != null) {
            Log.d(DEBUG_TAG, "given path " + path);
            currentPath.setText(path);
        }
        if (!(new File(path).exists())) {
            return;
        } else {
            try {
                File gPath = new File(path);
                FileModel parentFile = new FileModel();
                parentFile.setName("Back");
                parentFile.setDesc("back to " + gPath.getParentFile().getName());
                parentFile.setCheckPossible(false);
                parentFile.setFolder(true);
                parentFile.setAbsPath(gPath.getParentFile().getAbsolutePath());
                fileObjects.add(parentFile);
                File[] allDirectorFiles = gPath.listFiles();
                Arrays.sort(allDirectorFiles, new Comparator<File>() {
                    public int compare(File f1, File f2) {
                        return Long.valueOf(f2.lastModified()).compareTo(f1.lastModified());
                    }
                });
                for (File f : allDirectorFiles) {
                    // Log.d(DEBUG_TAG,"file path "+f.getAbsolutePath());
                    FileModel cFileModel = new FileModel();
                    cFileModel.setName(f.getName());
                    String type = GetPathMimeType.getMimeType(f.getAbsolutePath());
                    if (type.contains("null"))
                        type = "";
                    cFileModel.setDesc(type);
                    cFileModel.setMimeType(type);
                    cFileModel.setFolder(f.isDirectory());
                    cFileModel.setAbsPath(f.getAbsolutePath());
                    if(f.isDirectory()){
                        cFileModel.setCheckPossible(true);
                    }
                    else if (type == null)
                        cFileModel.setCheckPossible(false);
                    else {
                        if (type.toLowerCase().contains("image"))
                            cFileModel.setCheckPossible(true);
                        else
                            cFileModel.setCheckPossible(false);
                    }
                    fileObjects.add(cFileModel);
                }
            } catch (Exception e) {
                Log.d(DEBUG_TAG, "Error while getting processing filelist " + path.toString() + " ->e-> " + e.toString());
                currentPath.setText("Error Cant Index \"" + path + "\" location, probably no permission");
                initialPath();
                return;
            }
        }
        fileListDisplayAdapter.notifyDataSetChanged();
    }

    private void initialPath() {
        onInitialPath = true;
        fileObjects.clear();
        if (isSdPresent()) {
            FileModel externalCard = new FileModel();
            externalCard.setName("External SD Card");
            externalCard.setDesc("External SD Card");
            externalCard.setCheckPossible(false);
            externalCard.setFolder(true);
            File extPath = Environment.getExternalStorageDirectory();
            externalCard.setAbsPath(extPath.getAbsolutePath());
            fileObjects.add(externalCard);
        }
        FileModel internalCard = new FileModel();
        internalCard.setName("Internal SD Card");
        internalCard.setDesc("Internal SD Card");
        internalCard.setCheckPossible(false);
        internalCard.setFolder(true);
        File intPath = Environment.getDataDirectory();
        internalCard.setAbsPath(intPath.getAbsolutePath());
        fileObjects.add(internalCard);

        FileModel rootFile = new FileModel();
        rootFile.setName("Root");
        rootFile.setDesc("Root");
        rootFile.setAbsPath("/storage");
        rootFile.setCheckPossible(false);
        rootFile.setFolder(true);
        fileObjects.add(rootFile);
        for (FileModel currentFile : fileObjects) {
            Log.d(DEBUG_TAG, currentFile.getName());
        }
        fileListDisplayAdapter.notifyDataSetChanged();
    }

    public static boolean isSdPresent() {
        return android.os.Environment.getExternalStorageState().equals(
                android.os.Environment.MEDIA_MOUNTED);
    }

    @Override
    public boolean addPathToDB(String path,Boolean status,int listNum) {
        File f = new File(path);
        if(!f.exists())
            return false;
        if(f.isDirectory()){
            AddFoldersImagesToTableOnRequest addFoldersImagesToTableOnRequest = new AddFoldersImagesToTableOnRequest(this.getActivity());
            addFoldersImagesToTableOnRequest.execute(f, listNum,status);
            return true;
        }else{
            MyDataSource myDataSource = new MyDataSource();
            boolean state = myDataSource.addPath(context,path,status,listNum);
            if(state){
                Toast.makeText(context,"Added Image "+
                        f.getName()+
                        " Successfully to Rotation on List "+
                        listNum,Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(context,"Removed Image "+
                        f.getName()+
                        " Successfully From Rotation on List "+
                        listNum,Toast.LENGTH_SHORT).show();
            }
            return state;
        }
    }

    class AddFoldersImagesToTableOnRequest extends AsyncTask<Object, Object, Object> {
        private int total_counter;
        private Integer listNum;
        private ProgressDialog dialog;

        public AddFoldersImagesToTableOnRequest(Activity activity){
            dialog = new ProgressDialog(activity);
            total_counter = 0;
            listNum = 0;
        }

        @Override
        protected void onPreExecute(){
            this.dialog.setMessage("Adding Images in Folder to database Please Wait");
            this.dialog.setTitle("Please Wait");
            this.dialog.setCancelable(false);
            this.dialog.setCanceledOnTouchOutside(false);
            if(!this.dialog.isShowing())
                this.dialog.show();
        }

        @Override
        protected Object doInBackground(Object... objects) {
            File f = (File)objects[0];
            this.listNum = (Integer)objects[1];
            Boolean status = (Boolean)objects[2];
            if(status && AllConfigs.TRAIL_APP){
                if(MyDataSource.getRowEnabledCount(context) >= AllConfigs.TRAIL_APP_COUNT){
                    Log.d(AllConfigs.DEBUG_TAG,"failed to add path");
                    return false;
                }
            }
            synchronized (MyDataSource.Lock) {
                Integer status_int;
                if(status)
                    status_int = 1;
                else
                    status_int = 0;
                total_counter = 0;
                Collection<File> currentDirFiles = FileUtils.listFiles(
                        f,
                        new RegexFileFilter("^(.*?)"),
                        DirectoryFileFilter.DIRECTORY
                );
                String[] columns = {ImagesListTable.C_HASH, ImagesListTable.C_ACTUAL_IMG_PATH, ImagesListTable.C_CROP_IMG_PATH, ImagesListTable.C_ENABLE_FLAG, ImagesListTable.C_LIST_NO,ImagesListTable.C_IS_FOLDER};
                String sql = MyDataSource.createInsertQuery(ImagesListTable.T_TABLE_NAME, columns);
                DbHelper dbHelper = new DbHelper(context);
                SQLiteDatabase database = dbHelper.getWritableDatabase();
                SQLiteStatement statement = database.compileStatement(sql);
                // bulk insert starts here
                database.beginTransaction();
                statement.clearBindings();
                statement.bindLong(1, f.getAbsolutePath().hashCode());
                statement.bindString(2, f.getAbsolutePath());
                statement.bindString(3, f.getAbsolutePath());
                statement.bindLong(4, status_int);
                statement.bindLong(5, listNum);
                statement.bindLong(6,1);
                statement.execute();
                for (File cf : currentDirFiles) {
                    boolean isImage = GetPathMimeType.getMimeType(cf.getAbsolutePath()).toLowerCase().contains("image");
                   // Log.d(AllConfigs.DEBUG_TAG,"---- "+cf.getAbsolutePath()+" "+isImage);
                    if (isImage) {
                        statement.clearBindings();
                        statement.bindLong(1, cf.getAbsolutePath().hashCode());
                        statement.bindString(2, cf.getAbsolutePath());
                        statement.bindString(3, cf.getAbsolutePath());
                        statement.bindLong(4, status_int);
                        statement.bindLong(5, listNum);
                        statement.bindLong(6,0);
                        statement.execute();
                        total_counter = total_counter + 1;
                    }
                }
                database.setTransactionSuccessful();
                database.endTransaction();
                Log.d(AllConfigs.DEBUG_TAG, "added images count " + total_counter);
                return true;
            }
        }

        @Override
        protected void onPostExecute(Object result) {
            Boolean status = (Boolean) result;
            Log.d(AllConfigs.DEBUG_TAG,"Adding folder received status : "+status);
            if(dialog.isShowing())
                dialog.dismiss();
            if(status)
                Toast.makeText(context,"added/removed images "+total_counter+" to list "+listNum,Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(context,AllConfigs.TRAIL_FAILED_MESSAGE,Toast.LENGTH_LONG).show();
        }
    }
}
