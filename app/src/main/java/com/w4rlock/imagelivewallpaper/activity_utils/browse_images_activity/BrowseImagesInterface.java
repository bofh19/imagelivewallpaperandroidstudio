package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity;

/**
 * Created by saikrishna on 11/18/13.
 */
public interface BrowseImagesInterface {
    public void showLoadingDialog(String message,String title);
    public void cancelLoadingDialog();
}
