package com.w4rlock.imagelivewallpaper.activity_utils.models;

import java.util.Map;

/**
 * Created by skarumuru on 10/3/13.
 */
public class FileModel {
    private Boolean folder;
    private String name;
    private String absPath;
    private String Desc;
    private String mimeType;
    private Map<Integer,Boolean> checkBoxStatus;
    private Boolean checkPossible;

    public FileModel(){

        checkPossible = true;
    }

    public Boolean getCheckPossible() {
        return checkPossible;
    }

    public void setCheckPossible(Boolean checkPossible) {
        this.checkPossible = checkPossible;
    }

    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public Boolean getFolder() {
        return folder;
    }
    public void setFolder(Boolean folder) {
        this.folder = folder;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAbsPath() {
        return absPath;
    }

    public void setAbsPath(String absPath) {
        this.absPath = absPath;
    }

    public String getDesc() {
        return Desc;
    }

    public void setDesc(String desc) {
        Desc = desc;
    }

    public Map<Integer, Boolean> getCheckBoxStatus() {
        return checkBoxStatus;
    }

    public void setCheckBoxStatus(Map<Integer, Boolean> checkBoxStatus) {
        this.checkBoxStatus = checkBoxStatus;
    }
}