package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.ImagesListView;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.w4rlock.imagelivewallpaper.R;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.IntentVariablesConfig;
import com.w4rlock.imagelivewallpaper.activity_utils.models.EnabledPath;
import com.w4rlock.imagelivewallpaper.activity_utils.utils.GetDisplayParams;
import com.w4rlock.imagelivewallpaper.provider.MyDataSource;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

import eu.janmuller.android.simplecropimage.CropImage;

/**
 * Created by skarumuru on 10/4/13.
 */
public class ImagesListViewEachFragment extends Fragment {
    public static final String ARG_SECTION_NUMBER = "section_number";
    private ViewGroup rootView = null;
    ImagesListViewEachFragmentAdapter adapter=null;
    Dialog dialog;
    Integer listNo;
    SharedPreferences enableListSettings;
    private Context ctx;
    private Activity activity;
    public static GridView gridView;
    public ImagesListViewEachFragment(){

    }
    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState){
        rootView = (ViewGroup)inflater.inflate(R.layout.images_list_view_each_fragment_layout,container,false);
        enableListSettings =  this.getActivity().getApplicationContext().getSharedPreferences(IntentVariablesConfig.imageLiveWallpaperListSettings, 0);
        listNo = getArguments().getInt(ARG_SECTION_NUMBER);
        gridView = (GridView)rootView.findViewById(R.id.images_list_view_each_fragment_grid_view);
        ctx = this.getActivity().getApplicationContext();
        activity = this.getActivity();
        displayItems(listNo);
        dialog = new Dialog(this.getActivity().getApplicationContext());
        CheckBox enableListCheckBox = (CheckBox) rootView.findViewById(R.id.images_list_view_each_fragment_enable_list_checkbox);
        enableListCheckBox.setChecked(getCheckBoxStatus());
        enableListCheckBox.setOnCheckedChangeListener(mEnableCheckBoxChangedListener);
        return rootView;
    }

    private boolean getCheckBoxStatus() {
        Set<String> enabledLists = enableListSettings.getStringSet(IntentVariablesConfig.imageLiveWallpaperEnabledListsVariable,new TreeSet<String>());
        for (String eachEnabledList:enabledLists){
            if(listNo.equals(Integer.valueOf(eachEnabledList))){
                return true;
            }
        }
        return false;
    }

    private void displayItems(Integer listNo) {
        LinearLayout  loadingLayout = (LinearLayout) rootView.findViewById(R.id.images_list_view_each_fragment_loading_layout);
        try {
            loadingLayout.setVisibility(View.VISIBLE);
        }catch (Exception e){
            Log.d(AllConfigs.DEBUG_TAG,"tried to display loading layout but failed "+e.toString());
        }
        SetImagesGridItems setImagesGridItems = new SetImagesGridItems(this.getActivity().getApplicationContext());
        setImagesGridItems.execute(listNo);
    }

    private void drawGridView(ArrayList<EnabledPath> listImagePaths) {
        adapter = new ImagesListViewEachFragmentAdapter(this.getActivity().getApplicationContext(),R.layout.images_list_view_each_fragment_item_layout,listImagePaths);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(mItemClickedHandler);
        gridView.setOnItemLongClickListener(mItemLongClickedHandler);
    }

    // Listeners
    protected static final int PHOTO_CROPPED = 0;
    private AdapterView.OnItemClickListener mItemClickedHandler = new AdapterView.OnItemClickListener() {
        public void onItemClick(AdapterView<?> parents, View arg1, int arg2,
                                long arg3) {
            EnabledPath selectedItem = (EnabledPath) parents.getAdapter().getItem(arg2);
//            Intent image_intent = new Intent();
//            image_intent.setAction(Intent.ACTION_VIEW);
//            image_intent.setDataAndType(Uri.fromFile(new File(selectedItem.getPath())), "image/*");
//            startActivity(image_intent);
            int device_width =  GetDisplayParams.getDeviceWidth(ctx);
            int device_height = GetDisplayParams.getDeviceHeight(ctx);
            Log.d(AllConfigs.DEBUG_TAG,"=====device_height "+device_height);
            Log.d(AllConfigs.DEBUG_TAG,"=====device_width "+device_width);
            Intent intent = new Intent(activity,CropImage.class);
            //intent.setDataAndType(Uri.fromFile(new File(selectedItem.getPath())), "image/*");
            intent.putExtra(CropImage.IMAGE_PATH,selectedItem.getPath());
            intent.putExtra("crop", true);
            intent.putExtra("outputX",device_width);
            intent.putExtra("outputY",device_height);
            intent.putExtra("aspectX",device_width);
            intent.putExtra("aspectY",device_height);
            intent.putExtra(CropImage.SCALE_UP_IF_NEEDED,true);
            intent.putExtra("scale",true);
            intent.putExtra("return-data",false);
            intent.putExtra(CropImage.SAVE_IMAGE_PATH,getTempFile(selectedItem).getAbsolutePath().toString());
            intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
            intent.putExtra(CropImage.ENABLED_PATH_ITEM, (Serializable) selectedItem);
            startActivityForResult(intent, PHOTO_CROPPED);
        }
    };
//
//    private Uri getTempUri(){
//        return Uri.fromFile(getTempFile());
//    }

    private File getTempFile(EnabledPath selectedItem) {
        File f1 = new File(Environment.getExternalStorageDirectory(),"ImageLiveWallPapersCropped/");
        try {
            f1.mkdirs();
        }catch (Exception e){
            Log.d(AllConfigs.DEBUG_TAG,"failed to create directories "+e.toString());
        }
        File f = new File(f1.getPath(),selectedItem.getPath().hashCode()+".jpg");
        try {
            if(f.exists())
                f.delete();
            f.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return f;
    }

    private AdapterView.OnItemLongClickListener mItemLongClickedHandler = new AdapterView.OnItemLongClickListener(){

        @Override
        public boolean onItemLongClick(AdapterView<?> parents, View view, int i, long l) {
            EnabledPath selectedItem = (EnabledPath)parents.getAdapter().getItem(i);
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Path "+selectedItem.getPath());
            builder.setTitle("Item Path Details");
            dialog = builder.create();
            dialog.setCancelable(true);
            dialog.show();
            return true;
        }
    };

    private CompoundButton.OnCheckedChangeListener mEnableCheckBoxChangedListener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
            SharedPreferences.Editor editor = enableListSettings.edit();
            Set<String> enabledLists = new TreeSet<String>(enableListSettings.getStringSet(IntentVariablesConfig.imageLiveWallpaperEnabledListsVariable,new TreeSet<String>()));
            if(b)
                enabledLists.add(String.valueOf(listNo));
            else
                try {
                    enabledLists.remove(String.valueOf(listNo));
                }catch (Exception e){
                    Log.d(AllConfigs.DEBUG_TAG,"tried to remove list "+listNo+" got error "+e.toString());
                }
            editor.putStringSet(IntentVariablesConfig.imageLiveWallpaperEnabledListsVariable,enabledLists);
            editor.commit();
        }
    };


 //photo cropped given result
   @Override
   public void onActivityResult(int requestCode, int resultCode, Intent data){
       super.onActivityResult(requestCode,resultCode,data);

       switch (requestCode){
           case PHOTO_CROPPED:
               if(data == null){
                   Log.d(AllConfigs.DEBUG_TAG,"====== crop activity finished but got data as null ");
                   return;
               }
               final Bundle extras = data.getExtras();
               if(extras != null){
                   String path = data.getStringExtra(CropImage.IMAGE_PATH);
                   String cropPath = data.getStringExtra(CropImage.SAVE_IMAGE_PATH).toString();
                   EnabledPath givenItem = (EnabledPath) data.getSerializableExtra(CropImage.ENABLED_PATH_ITEM);
                   MyDataSource myDataSource = new MyDataSource();
                   cropPath = cropPath.replace("file:///","/");
                   myDataSource.addPath(this.getActivity().getApplicationContext(),givenItem.getPath(),cropPath,givenItem.getEnabledStatus(),givenItem.getListNo());
                   Log.d(AllConfigs.DEBUG_TAG,"----- given list no is ---- "+givenItem.getListNo());
                   Log.d(AllConfigs.DEBUG_TAG,"====== seems all is good checkout file at "+data.getStringExtra(CropImage.SAVE_IMAGE_PATH).toString());
                   Log.d(AllConfigs.DEBUG_TAG,"====== seems all is good crop path is file at "+cropPath);
                   Log.d(AllConfigs.DEBUG_TAG,"====== seems all is good enabled crop path is "+givenItem.getPath());
                   Toast.makeText(this.getActivity().getApplicationContext(),"Sucessfully cropped and saved in cache reload to check thumbnail",Toast.LENGTH_SHORT);
               }
       }
   }



//    private void showHideDisplayLoaders(boolean b) {
//        try{
//            ProgressBar progessBar = (ProgressBar) this.getActivity().findViewById(R.id.images_list_view_progress_bar);
//            TextView loadingTextView = (TextView) this.getActivity().findViewById(R.id.images_list_view_reloading_images_text);
//            if(b){
//                progessBar.setVisibility(View.VISIBLE);
//                loadingTextView.setVisibility(View.VISIBLE);
//            }
//            else{
//                progessBar.setVisibility(View.GONE);
//                loadingTextView.setVisibility(View.GONE);
//            }
//        }
//        catch(Exception e){
//            Log.d(AllConfigs.DEBUG_TAG, "error unable to get progressBar");
//        }
//    }

    class SetImagesGridItems extends AsyncTask{
        Context context;
        public SetImagesGridItems(Context context){
          this.context = context;
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            Integer listNo = (Integer)objects[0];
            MyDataSource myDataSource = new MyDataSource();
            ArrayList<EnabledPath> listImagePaths = myDataSource.getImagesListValues(context, listNo);
            return listImagePaths;
        }

        @Override
        public void onPostExecute(Object result){
            LinearLayout loadingLayout = (LinearLayout) rootView.findViewById(R.id.images_list_view_each_fragment_loading_layout);
            try {
                loadingLayout.setVisibility(View.GONE);
            }catch (Exception e){
                Log.d(AllConfigs.DEBUG_TAG,"tried to hide loading layout but failed "+e.toString());
            }
            ArrayList<EnabledPath> listImagePaths = (ArrayList<EnabledPath>)(result);
            drawGridView(listImagePaths);
        }
    }
}
