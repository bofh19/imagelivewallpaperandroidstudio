package com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.ImagesListView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageLoadingListener;
import com.nostra13.universalimageloader.core.assist.SimpleImageLoadingListener;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.w4rlock.imagelivewallpaper.R;
import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.models.EnabledPath;
import com.w4rlock.imagelivewallpaper.provider.MyDataSource;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by saikrishna on 11/4/13.
 */
public class ImagesListViewEachFragmentAdapter extends ArrayAdapter<EnabledPath> {

    private Context context;
    ArrayList<EnabledPath> data;
    private int layoutResourceId;
    private ImageLoader imageLoader;
    private DisplayImageOptions displayImageOptions;
    private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    public ImagesListViewEachFragmentAdapter(Context context,int layoutResourceId,ArrayList<EnabledPath> data){
        super(context,layoutResourceId,data);
        this.context = context;
        this.data = data;
        this.layoutResourceId = layoutResourceId;
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context).build();
        imageLoader = ImageLoader.getInstance();
        imageLoader.init(config);

        displayImageOptions = new DisplayImageOptions.Builder()
                                .showStubImage(R.drawable.ic_stub)
                                .showImageForEmptyUri(R.drawable.ic_empty)
                                .showImageOnFail(R.drawable.ic_error)
                                .build();
    }

    @Override
    public View getView(final int position,View convertView,ViewGroup parent){
        View row = convertView;
        ImgHolder imgHolder = null;
        if(row == null){
            LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layoutResourceId,parent,false);
            imgHolder = new ImgHolder();
            imgHolder.img = (ImageView)row.findViewById(R.id.images_list_view_each_fragment_imgView);
            imgHolder.cropImg = (ImageView)row.findViewById(R.id.images_list_view_each_fragment_img_button_1);
            imgHolder.removeImg = (ImageView)row.findViewById(R.id.images_list_view_each_fragment_img_button_2);
            row.setTag(imgHolder);
        }else{
            imgHolder = (ImgHolder) row.getTag();
        }
        imgHolder.cropImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startCropActivity(view,position);
            }
        });
        imgHolder.removeImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startDisableActivity(view,position);
            }
        });
        try{
            String path = data.get(position).getCropImgPath();
            imageLoader.displayImage("file:///"+path,imgHolder.img,displayImageOptions,animateFirstListener);
        }catch (Exception e){
            Log.d(AllConfigs.DEBUG_TAG, "something failed in each images list view fragments adapter " + e.toString());
        }

        try {
            if(!data.get(position).getEnabledStatus()){
                Log.d(AllConfigs.DEBUG_TAG,"enabled status was set false");
                imgHolder.img.setColorFilter(Color.rgb(123, 122, 122), PorterDuff.Mode.LIGHTEN);
            }
            else{
                Log.d(AllConfigs.DEBUG_TAG,"enabled status was set to true");
                imgHolder.img.setColorFilter(null);
            }

        }catch (Exception e){
            Log.d(AllConfigs.DEBUG_TAG, "something failed while trying to enable/disable relative layout " + e.toString());
        }
        return row;
    }

    private void startDisableActivity(View view, int position) {
        Log.d(AllConfigs.DEBUG_TAG,"clicked on disable "+data.get(position).getPath());
        MyDataSource myDataSource = new MyDataSource();
        myDataSource.disableEnableCurrentPath(context, data.get(position), data.get(position).getListNo());
        ViewGroup parent = (ViewGroup) view.getParent().getParent();
        ImageView imgView = (ImageView)parent.findViewById(R.id.images_list_view_each_fragment_imgView);
        if(!data.get(position).getEnabledStatus()){
            imgView.setColorFilter(null);
        }else{
            imgView.setColorFilter(Color.rgb(123, 122, 122), PorterDuff.Mode.LIGHTEN);
        }
        data.get(position).setEnabledStatus(!data.get(position).getEnabledStatus());
    }

    private void startCropActivity(View view, int position) {
        Log.d(AllConfigs.DEBUG_TAG,"clicked on crop "+data.get(position).getPath());

        ImagesListViewEachFragment.gridView.performItemClick(
                ImagesListViewEachFragment.gridView.getAdapter().getView(position,null,null),
                position,
                ImagesListViewEachFragment.gridView.getAdapter().getItemId(position));
    }

    static class ImgHolder{
        ImageView img;
        ImageView cropImg;
        ImageView removeImg;
    }

    private static class AnimateFirstDisplayListener extends SimpleImageLoadingListener{
        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());
        @Override
        public void onLoadingComplete(String imageUri,View view,Bitmap loadedImage){
            if(loadedImage != null){
                ImageView imageView = (ImageView)view;
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if(firstDisplay){
                    FadeInBitmapDisplayer.animate(imageView,500);
                    displayedImages.add(imageUri);
                }
            }
        }
    }
}
