package com.w4rlock.imagelivewallpaper.provider;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.w4rlock.imagelivewallpaper.provider.tables.ImagesListTable;

/**
 * Created by skarumuru on 10/4/13.
 */
public class DbHelper extends SQLiteOpenHelper {
    public static final String DB_NAME = "selection_db";
    public static final int DB_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DB_NAME,null,DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(ImagesListTable.onCreate());
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}
