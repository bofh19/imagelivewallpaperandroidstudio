package com.w4rlock.imagelivewallpaper.provider;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.models.EnabledPath;
import com.w4rlock.imagelivewallpaper.activity_utils.utils.GetPathMimeType;
import com.w4rlock.imagelivewallpaper.livewallpaper.ImagePathModel;
import com.w4rlock.imagelivewallpaper.provider.tables.ImagesListTable;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by skarumuru on 10/10/13.
 */
public class MyDataSource {
    public final static String Lock = "dblock";

    public Boolean addPath(Context context, String userPath, Boolean enableFlagStatus, int listNum) {
        if(enableFlagStatus && AllConfigs.TRAIL_APP){
            if(getRowEnabledCount(context) >= AllConfigs.TRAIL_APP_COUNT){
                return false;
            }
        }
        synchronized (Lock) {
          //  add a new path or updates it if already exists
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ImagesListTable.C_HASH, userPath.hashCode());
            contentValues.put(ImagesListTable.C_ENABLE_FLAG, getBooleanNumber(enableFlagStatus));
            contentValues.put(ImagesListTable.C_ACTUAL_IMG_PATH, userPath);
            contentValues.put(ImagesListTable.C_CROP_IMG_PATH, userPath);
            contentValues.put(ImagesListTable.C_LIST_NO, listNum);
            if((new File(userPath).isDirectory())){
                contentValues.put(ImagesListTable.C_IS_FOLDER,1);
            }
            long returnId = database.insertWithOnConflict(ImagesListTable.T_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            database.close();
            if (returnId == -1)
                return false;
            return true;
        }
    }

    public Boolean addPath(Context context, String userPath,String cropPath, Boolean enableFlagStatus, int listNum) {
        if(enableFlagStatus && AllConfigs.TRAIL_APP){
            if(getRowEnabledCount(context) >= AllConfigs.TRAIL_APP_COUNT){
                return false;
            }
        }
        synchronized (Lock) {
            //  add a new path or updates it if already exists
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            ContentValues contentValues = new ContentValues();
            contentValues.put(ImagesListTable.C_HASH, userPath.hashCode());
            contentValues.put(ImagesListTable.C_ENABLE_FLAG, getBooleanNumber(enableFlagStatus));
            contentValues.put(ImagesListTable.C_ACTUAL_IMG_PATH, userPath);
            contentValues.put(ImagesListTable.C_CROP_IMG_PATH, cropPath);
            contentValues.put(ImagesListTable.C_LIST_NO, listNum);
            if((new File(userPath).isDirectory())){
                contentValues.put(ImagesListTable.C_IS_FOLDER,1);
            }
            long returnId = database.insertWithOnConflict(ImagesListTable.T_TABLE_NAME, null, contentValues, SQLiteDatabase.CONFLICT_REPLACE);
            database.close();
            if (returnId == -1)
                return false;
            return true;
        }
    }

    public static int getRowEnabledCount(Context context){
        synchronized (Lock){
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            String finalQuery;
            Cursor resultSet;
            Log.d(AllConfigs.DEBUG_TAG,"Calculating count of rows that are enabled");
            finalQuery = "select * from "+ImagesListTable.T_TABLE_NAME+" where "+ImagesListTable.C_ENABLE_FLAG+" = 1 and "+ImagesListTable.C_IS_FOLDER +" = 0;";
            Log.d(AllConfigs.DEBUG_TAG,"table query "+finalQuery);
            resultSet = database.rawQuery(finalQuery,null);
            int rowCount = resultSet.getCount();
            Log.d(AllConfigs.DEBUG_TAG,"count: "+rowCount);
            return rowCount;
        }
    }

    public Boolean getStatus(Context context, String userPath, int listNum) {
        synchronized (Lock) {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            String[] tableCols = {ImagesListTable.C_HASH, ImagesListTable.C_ACTUAL_IMG_PATH, ImagesListTable.C_ENABLE_FLAG, ImagesListTable.C_LIST_NO};
            String selectionClause = ImagesListTable.C_HASH + " = ? and " + ImagesListTable.C_LIST_NO + " = ?";
            String[] selectionArgs = {String.valueOf(userPath.hashCode()), String.valueOf(listNum)};
            Cursor checkExisting = database.query(ImagesListTable.T_TABLE_NAME, tableCols, selectionClause, selectionArgs, null, null, null);
            if (checkExisting == null) {
                database.close();
                return false;
            } else if (checkExisting.getCount() == 0) {
                database.close();
                return false;
            } else if (checkExisting.getCount() == 1) {
                checkExisting.moveToFirst();
                Boolean returnValue = getNumberBoolean(checkExisting.getInt(2));
                database.close();
                return returnValue;
            } else {
                database.close();
                return false;
            }
        }
    }

    public ArrayList<EnabledPath> getAllValues(Context context) {
        synchronized (Lock) {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            ArrayList<EnabledPath> allEnabledValues = new ArrayList<EnabledPath>();
            String[] tableCols = {ImagesListTable.C_HASH, ImagesListTable.C_ACTUAL_IMG_PATH, ImagesListTable.C_CROP_IMG_PATH,ImagesListTable.C_ENABLE_FLAG, ImagesListTable.C_LIST_NO};
            //String selectionClause = ImagesListTable.C_ENABLE_FLAG + " = ?"+" and "+ImagesListTable.C_LIST_NO+" = ?";
            //String[] selectionArgs = {String.valueOf(getBooleanNumber(true)),String.valueOf(listNo)};

            Cursor resultSet = database.query(ImagesListTable.T_TABLE_NAME, tableCols, null, null, null, null, null);
            if (resultSet == null) {
                database.close();
                return allEnabledValues;
            }
            if (resultSet.getCount() == 0) {
                database.close();
                return allEnabledValues;
            }
//            String c[] = resultSet.getColumnNames();
//            for(String cx:c){
//                Log.d(AllConfigs.DEBUG_TAG," column "+cx+" index is "+resultSet.getColumnIndex(cx));
//            }
            if (resultSet.moveToFirst()) {
                do {
                    EnabledPath currentEnabledPath = new EnabledPath(
                            resultSet.getString(resultSet.getColumnIndex(ImagesListTable.C_ACTUAL_IMG_PATH)),
                            resultSet.getInt(resultSet.getColumnIndex(ImagesListTable.C_LIST_NO)),
                            getNumberBoolean(resultSet.getInt(resultSet.getColumnIndex(ImagesListTable.C_ENABLE_FLAG))),
                            resultSet.getString(resultSet.getColumnIndex(ImagesListTable.C_CROP_IMG_PATH))
                    );
                    //if(GetPathMimeType.getMimeType(currentEnabledPath.getPath()).contains("image"))
                    allEnabledValues.add(currentEnabledPath);
                } while (resultSet.moveToNext());
            }
            database.close();
            return allEnabledValues;
        }
    }


    // used to get list of all paths enabled or disabled in a given list no
    public ArrayList<EnabledPath> getImagesListValues(Context context,int listNo){
        synchronized (Lock) {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            ArrayList<EnabledPath> allEnabledValues = new ArrayList<EnabledPath>();
            String[] tableCols = {ImagesListTable.C_HASH, ImagesListTable.C_ACTUAL_IMG_PATH, ImagesListTable.C_CROP_IMG_PATH,ImagesListTable.C_ENABLE_FLAG, ImagesListTable.C_LIST_NO};
            //String selectionClause = ImagesListTable.C_ENABLE_FLAG + " = ?"+" and "+ImagesListTable.C_LIST_NO+" = ?";
            String selectionClause = ImagesListTable.C_LIST_NO + " = ? and "+ImagesListTable.C_IS_FOLDER+" = 0 ";
            //String[] selectionArgs = {String.valueOf(getBooleanNumber(true)),String.valueOf(listNo)};
            String[] selectionArgs = {String.valueOf(listNo)};
            Cursor resultSet = database.query(ImagesListTable.T_TABLE_NAME, tableCols, selectionClause, selectionArgs, null, null, ImagesListTable.C_HASH);
            if (resultSet == null) {
                database.close();
                return allEnabledValues;
            }
            if (resultSet.getCount() == 0) {
                database.close();
                return allEnabledValues;
            }
//            String c[] = resultSet.getColumnNames();
//            for(String cx:c){
//                Log.d(AllConfigs.DEBUG_TAG," column "+cx+" index is "+resultSet.getColumnIndex(cx));
//            }
            if (resultSet.moveToFirst()) {
                do {
                    EnabledPath currentEnabledPath = new EnabledPath(
                                        resultSet.getString(resultSet.getColumnIndex(ImagesListTable.C_ACTUAL_IMG_PATH)),
                                        resultSet.getInt(resultSet.getColumnIndex(ImagesListTable.C_LIST_NO)),
                                        getNumberBoolean(resultSet.getInt(resultSet.getColumnIndex(ImagesListTable.C_ENABLE_FLAG))),
                                        resultSet.getString(resultSet.getColumnIndex(ImagesListTable.C_CROP_IMG_PATH))
                                        );
                    if(GetPathMimeType.getMimeType(currentEnabledPath.getPath()).contains("image"))
                        allEnabledValues.add(currentEnabledPath);
                } while (resultSet.moveToNext());
            }
            database.close();
            return allEnabledValues;
        }
    }

    // this method is used in live wallpaper image rotation. don't touch if you are an idiot
    // return an image_path on requested row number
    // if row num doesn't exist or is over the row count or less then 0 tries to return row 1
    // if row 1 also doesn't exist then returns an ImagePathModel that has noData set to true
    public ImagePathModel getImagePath(Context context,ArrayList<Integer> listNo,int rowNum,int direction){
        synchronized (Lock) {
            // creating default empty image path model
            ImagePathModel emptyImagePathModel = new ImagePathModel();
            emptyImagePathModel.setImagePath("");
            emptyImagePathModel.setCurrentRowId(-1);
            emptyImagePathModel.setNoData(true);
            if(listNo.size() == 0){ // if no list is given return empty image path model
                Log.d(AllConfigs.DEBUG_TAG,"RETURNED BECAUSE NO LIST WAS GiVEN ON INPUT e");
                return emptyImagePathModel;
            }
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getReadableDatabase();
            String numbers=null;
            for(Integer ict: listNo){  // converting given list numbers for sql query "IN" format
                if(numbers == null){
                    numbers = String.valueOf(ict)+",";
                }else{
                    numbers = numbers+ict+",";
                }
            }
            numbers = numbers.substring(0,numbers.length()-1);
            Log.d(AllConfigs.DEBUG_TAG," --- given row num "+rowNum);
            rowNum = rowNum - 1;
            String finalQuery;
            Cursor resultSet;
            if(rowNum < 0 && direction == -1){  // a back swipe was given while requesting a negative row id. so making row number equal to last row
                                                // essentially taking last row value and preparing it to return.
                Log.d(AllConfigs.DEBUG_TAG,"----- a back swipe was given while requesting a negative row id. row num < 0 and direction is backward so going to start on offset");
                finalQuery = "select * from "+ImagesListTable.T_TABLE_NAME+" where "+ImagesListTable.C_LIST_NO+" in ("+numbers+") and "+ImagesListTable.C_ENABLE_FLAG+" = 1 and "+ImagesListTable.C_IS_FOLDER +" = 0;";
                resultSet = database.rawQuery(finalQuery,null);
                if(resultSet.getCount() == 0){
                    Log.d(AllConfigs.DEBUG_TAG,"RETURNED BECAUSE NO ITEMS WERE FOUND 1 e");
                    return emptyImagePathModel;
                }
                int rowCount = resultSet.getCount();
                rowNum = rowCount - 1;
            }
            Log.d(AllConfigs.DEBUG_TAG,"numbers str is "+numbers);
            finalQuery = "select "+ImagesListTable.C_HASH+","+  // making an actual query
                    ImagesListTable.C_ACTUAL_IMG_PATH+","+
                    ImagesListTable.C_CROP_IMG_PATH+" from "+
                    ImagesListTable.T_TABLE_NAME+" where "+
                    ImagesListTable.C_ENABLE_FLAG +" = 1 and "+
                    ImagesListTable.C_IS_FOLDER +" = 0 and "+
                    ImagesListTable.C_LIST_NO+" in ("+numbers+") limit 1 offset "+rowNum+";";
            Log.d(AllConfigs.DEBUG_TAG," -- --- - - - final Query "+finalQuery);
            resultSet = database.rawQuery(finalQuery,null);
            if (resultSet == null) { // this will probably never execute
                database.close();
                Log.d(AllConfigs.DEBUG_TAG,"-----result set is null wtf >-< RETURNED e");
                return emptyImagePathModel;
            }
            if(resultSet.getCount() == 0){  // if count is still zero then we have no data
                if(rowNum >= 0){
                    Log.d(AllConfigs.DEBUG_TAG,"----- if count is still zero then we have no data row num < 0 and direction is backward so going to start on offset");
                    finalQuery = "select * from "+ImagesListTable.T_TABLE_NAME+" where "+ImagesListTable.C_LIST_NO+" in ("+numbers+") and "+ImagesListTable.C_ENABLE_FLAG+" = 1 and "+ImagesListTable.C_IS_FOLDER +" = 0;";
                    resultSet = database.rawQuery(finalQuery,null);
                    if(resultSet.getCount() == 0){
                        Log.d(AllConfigs.DEBUG_TAG,"RETURNED BECAUSE NO ITEMS WERE FOUND 2 e");
                        return emptyImagePathModel;
                    }
                    rowNum = 0;
                }
            }
            finalQuery = "select "+ImagesListTable.C_HASH+","+
                    ImagesListTable.C_ACTUAL_IMG_PATH+","+
                    ImagesListTable.C_CROP_IMG_PATH+" from "+
                    ImagesListTable.T_TABLE_NAME+" where "+
                    ImagesListTable.C_ENABLE_FLAG +" = 1 and "+
                    ImagesListTable.C_IS_FOLDER +" = 0 and "+
                    ImagesListTable.C_LIST_NO+" in ("+numbers+") limit 1 offset "+rowNum+";";
            Log.d(AllConfigs.DEBUG_TAG," -- --- - - - final Query "+finalQuery);
            resultSet = database.rawQuery(finalQuery,null);
            if(resultSet.getCount() == 0){
                Log.d(AllConfigs.DEBUG_TAG,"RETURNED BECAUSE NO ITEMS WERE FOUND 3 e");
                return emptyImagePathModel;
            }
            ImagePathModel requestedImagePath = new ImagePathModel();
            if (resultSet.moveToFirst()) {
                Log.d(AllConfigs.DEBUG_TAG," -- image paths : "+resultSet.getString(2));
                requestedImagePath.setImagePath(resultSet.getString(2));
                requestedImagePath.setCurrentRowId(rowNum+1);
                requestedImagePath.setNoData(false);
            }
            database.close();
            Log.d(AllConfigs.DEBUG_TAG,"RETURNED AN ITEM "+requestedImagePath.toString());
            return requestedImagePath;
        }
    }


//    public Boolean dropAndCreateImagesListTable(Context context){
//        synchronized (Lock){
//            DbHelper dbHelper = new DbHelper(context);
//            SQLiteDatabase database = dbHelper.getWritableDatabase();
//            try {
//                database.execSQL(ImagesListTable.dropTable());
//                database.execSQL(ImagesListTable.onCreate());
//                database.close();
//                return true;
//            }catch (Exception e){
//                Log.d(AllConfigs.DEBUG_TAG, "failed at drop and create images list table " + e.toString());
//                return false;
//            }finally {
//                if(database.isOpen())
//                    database.close();
//            }
//        }
//    }

    public static int getBooleanNumber(Boolean enableFlagStatus) {
        if (enableFlagStatus)
            return 1;
        else
            return 0;
    }

    public static Boolean getNumberBoolean(int number) {
        if (number == 1) {
            return true;
        } else if (number == 0) {
            return false;
        }
        return false;
    }

    static public String createInsertQuery(final String tableName, final String[] columnNames) {
        if (tableName == null || columnNames == null || columnNames.length == 0) {
            throw new IllegalArgumentException();
        }
        final StringBuilder s = new StringBuilder();
        s.append("INSERT INTO ").append(tableName).append(" (");
        for (String column : columnNames) {
            s.append(column).append(" ,");
        }
        int length = s.length();
        s.delete(length - 2, length);
        s.append(") VALUES( ");
        for (int i = 0; i < columnNames.length; i++) {
            s.append(" ? ,");
        }
        length = s.length();
        s.delete(length - 2, length);
        s.append(")");
        return s.toString();
    }

    public boolean removeCurrentPath(Context context, EnabledPath currentEnabledPath) {
        synchronized (Lock) {
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            return database.delete(ImagesListTable.T_TABLE_NAME,ImagesListTable.C_HASH + "="+currentEnabledPath.getPath().hashCode(),null) > 0;
        }
    }

    public  boolean removeCurrentPath(Context context,EnabledPath currentEnabledPath,int listNo){
        synchronized (Lock){
            DbHelper dbHelper = new DbHelper(context);
            SQLiteDatabase database = dbHelper.getWritableDatabase();
            return database.delete(ImagesListTable.T_TABLE_NAME,ImagesListTable.C_HASH + "="+currentEnabledPath.getPath().hashCode()+" and "+ImagesListTable.C_LIST_NO+" = "+listNo,null)>0;
        }
    }

    public void cleanUp(Context context){
        synchronized (Lock){
            ArrayList<EnabledPath> allEnabledPaths = getAllValues(context);
            for(EnabledPath currentEnabledPath:allEnabledPaths){
                boolean check = new File(currentEnabledPath.getPath()).exists();
                boolean check2 = new File(currentEnabledPath.getCropImgPath()).exists();
                Log.d(AllConfigs.DEBUG_TAG,"checking file "+currentEnabledPath.getPath()+" status "+check);
                if(!check){
                    boolean status = removeCurrentPath(context,currentEnabledPath);
                    Log.d(AllConfigs.DEBUG_TAG,"status on "+currentEnabledPath.getPath()+" "+status);
                }
                if(!check2 && check){
                       removeCurrentPath(context,currentEnabledPath);
                       addPath(context,currentEnabledPath.getPath(),currentEnabledPath.getPath(),currentEnabledPath.getEnabledStatus(),currentEnabledPath.getListNo());
                }
                if(!check2 && !check){
                    removeCurrentPath(context,currentEnabledPath);
                }
            }
        }
    }

    public boolean disableEnableCurrentPath(Context context, EnabledPath enabledPath, int listNo) {
        return addPath(context, enabledPath.getPath(), !enabledPath.getEnabledStatus(),listNo) ;
    }
}
