package com.w4rlock.imagelivewallpaper.provider.tables;

import android.util.Log;

import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;

/**
 * Created by skarumuru on 10/8/13.
 */
public class ImagesListTable {
    public static final String T_TABLE_NAME="IMAGES_LIST";
    public static final String C_HASH = "HASH";
    public static final String C_LIST_NO = "LIST_NO";
    public static final String C_ACTUAL_IMG_PATH = "ACTUAL_IMG_PATH";
    public static final String C_CROP_IMG_PATH = "CROP_IMG_PATH";
    public static final String C_ENABLE_FLAG = "ENABLE_FLAG";
    public static final String C_IS_FOLDER = "IS_FOLDER";
    public static final String constraint_hash_list = "unique_hash_list_constraint";
    public static String onCreate(){
        final String query = "CREATE TABLE " +T_TABLE_NAME+" "+
                             "(" +
                                "_ID INTEGER PRIMARY KEY AUTOINCREMENT," +
                                C_HASH+" INTEGER NOT NULL," +
                                C_ACTUAL_IMG_PATH+" TEXT NOT NULL," +
                                C_CROP_IMG_PATH+" TEXT," +
                                C_LIST_NO+" NUMBER DEFAULT 1,"+
                                C_ENABLE_FLAG+" BOOLEAN DEFAULT 1," +
                                C_IS_FOLDER + " BOOLEAN DEFAULT 0,"+
                                "CONSTRAINT " +constraint_hash_list+" "+
                                "UNIQUE ("+C_HASH+","+C_LIST_NO+") ON CONFLICT REPLACE"+
                             ");";
        Log.d(AllConfigs.DEBUG_TAG, " ---- create sql query " + query);
        return query;
    }
//    public static String dropTable(){
//        final String query = "DROP TABLE "+T_TABLE_NAME+";";
//        return query;
//    }
}
