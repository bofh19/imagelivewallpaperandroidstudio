package com.w4rlock.imagelivewallpaper;

import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
import com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.BrowseImagesInterface;
import com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.Configs;
import com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.FileBrowserFragment;
import com.w4rlock.imagelivewallpaper.activity_utils.browse_images_activity.ImagesListView.ImagesListViewMainFragment;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.WallpaperManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ArrayAdapter;

public class BrowseImagesActivity extends FragmentActivity implements
				ActionBar.OnNavigationListener,BrowseImagesInterface {
	
	private static final String STATE_SELECTED_NAVIGATION_ITEM = Configs.STATE_SELECTED_NAVIGATION_ITEM;

    private final static String TAG_FRAGMENT = Configs.FILE_BROWSE_TAG_FRAGMENT;
    private final static String IMAGE_LIST_TAG_FRAGMENT = Configs.IMAGE_LIST_TAG_FRAGMENT;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        //getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
		setContentView(R.layout.activity_browse_images);


		final ActionBar actionBar = getActionBar();
		actionBar.setDisplayShowTitleEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		actionBar.setListNavigationCallbacks(new ArrayAdapter<String>(actionBar.getThemedContext(),
				android.R.layout.simple_list_item_1,
				android.R.id.text1,
				new String[]{
					getString(R.string.main_nav_menu_file_browser),
					getString(R.string.main_nav_menu_list_of_images),	
				}), this);
	}
	
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState){
		if(savedInstanceState.containsKey(STATE_SELECTED_NAVIGATION_ITEM)){
			getActionBar().setSelectedNavigationItem(savedInstanceState.getInt(STATE_SELECTED_NAVIGATION_ITEM));
		}
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState){
		outState.putInt(STATE_SELECTED_NAVIGATION_ITEM, getActionBar().getSelectedNavigationIndex());
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_browse_images, menu);
		return true;
	}

    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.menu_choose_lwp:
                Intent menu_choose_lwp_intent = new Intent();
                menu_choose_lwp_intent.setAction(WallpaperManager.ACTION_LIVE_WALLPAPER_CHOOSER);
                startActivity(menu_choose_lwp_intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

	@Override
	public boolean onNavigationItemSelected(int position, long id) {
        if(position == 0){
            Fragment file_browse_fragment = new FileBrowserFragment();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, file_browse_fragment,TAG_FRAGMENT).addToBackStack(null).commit();
            return true;
        }else if(position == 1){
            Fragment images_list_view_fragment = new ImagesListViewMainFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.container, images_list_view_fragment,IMAGE_LIST_TAG_FRAGMENT).commit();
            return true;
        }
		return false;
	}

    @Override
    public void onBackPressed(){
        Log.d(AllConfigs.DEBUG_TAG, "back pressed");
        try{
            Log.d(AllConfigs.DEBUG_TAG, "back pressed inside try");
            final FileBrowserFragment file_browse_fragment = (FileBrowserFragment)getSupportFragmentManager().findFragmentByTag(TAG_FRAGMENT);
            final ImagesListViewMainFragment images_list_view_fragment = (ImagesListViewMainFragment)getSupportFragmentManager().findFragmentByTag(IMAGE_LIST_TAG_FRAGMENT);
            if(!file_browse_fragment.allowBackPressed()){
                Log.d(AllConfigs.DEBUG_TAG, "back pressed inside finish block");
                if(file_browse_fragment != null)
                    getSupportFragmentManager().beginTransaction().remove(file_browse_fragment).commit();
                if(images_list_view_fragment != null)
                    getSupportFragmentManager().beginTransaction().remove(images_list_view_fragment).commit();
                //super.onBackPressed();
                Log.d(AllConfigs.DEBUG_TAG, "back pressed before finish statement");
                finish();
//                Intent startMain = new Intent(Intent.ACTION_MAIN);
//                startMain.addCategory(Intent.CATEGORY_HOME);
//                startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(startMain);
            }
        }catch(Exception e){
            Log.d(AllConfigs.DEBUG_TAG, "back pressed inside exception "+e.toString());
            //super.onBackPressed();
            finish();
        }
    }



    @Override
    public void showLoadingDialog(String message,String title) {


    }

    @Override
    public void cancelLoadingDialog() {

    }
}
