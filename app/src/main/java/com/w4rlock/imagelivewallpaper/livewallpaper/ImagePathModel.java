package com.w4rlock.imagelivewallpaper.livewallpaper;

/**
 * Created by saikrishna on 11/7/13.
 */
public class ImagePathModel {
    private String imagePath;
    private int currentRowId;
    private Boolean noData;
    public ImagePathModel(){

    }

    @Override
    public String toString(){
        return getImagePath()+" - "+String.valueOf(getCurrentRowId())+" - "+String.valueOf(getNoData());
    }

    public Boolean getNoData() {
        return noData;
    }

    public void setNoData(Boolean noData) {
        this.noData = noData;
    }

    public int getCurrentRowId() {
        return currentRowId;
    }

    public void setCurrentRowId(int currentRowId) {
        this.currentRowId = currentRowId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
