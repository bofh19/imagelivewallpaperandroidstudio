//package com.w4rlock.imagelivewallpaper.livewallpaper;
//
//import android.content.Context;
//import android.content.SharedPreferences;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.graphics.Canvas;
//import android.graphics.Color;
//import android.graphics.Paint;
//import android.graphics.PorterDuff;
//import android.graphics.PorterDuffXfermode;
//import android.os.Handler;
//import android.service.wallpaper.WallpaperService;
//import android.util.DisplayMetrics;
//import android.util.Log;
//import android.view.MotionEvent;
//import android.view.SurfaceHolder;
//
//import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.AllConfigs;
//import com.w4rlock.imagelivewallpaper.activity_utils.all_activitys.IntentVariablesConfig;
//import com.w4rlock.imagelivewallpaper.activity_utils.utils.GetPathMimeType;
//import com.w4rlock.imagelivewallpaper.livewallpaper.utils.ScalingUtilities;
//import com.w4rlock.imagelivewallpaper.provider.MyDataSource;
//
//import java.io.File;
//import java.util.ArrayList;
//import java.util.Set;
//import java.util.TreeSet;
//
///**
// * Created by saikrishna on 11/7/13.
// */
//public class LiveWallpaperService extends WallpaperService {
//    public static final String prefs_name_var = IntentVariablesConfig.imageLiveWallpaperListSelectedVariable;
//    public static final String prefs_name_settings = IntentVariablesConfig.imageLiveWallpaperListSettings;
//    private Context context;
//    private SharedPreferences sharedPreferences;
//    @Override
//    public Engine onCreateEngine() {
//        sharedPreferences = this.getSharedPreferences(prefs_name_settings,0);
//        context = this.getApplicationContext();
//        return new ImageLiveWallpaperEngine();
//    }
//
//    private class ImageLiveWallpaperEngine extends Engine{
//        private boolean mVisible = false;
//        private final Handler mHandler = new Handler();
//        private final Runnable mUpdateDisplay = new Runnable() {
//            @Override
//            public void run() {
//                setTouchEventsEnabled(true);
//                setOffsetNotificationsEnabled(true);
//                draw();
//            }
//        };
//
//        @Override
//        public void onVisibilityChanged(boolean visible){
//            mVisible = visible;
//            if(visible){
//                draw();
//            }else {
//                mHandler.removeCallbacks(mUpdateDisplay);
//            }
//        }
//
//        @Override
//        public void onSurfaceChanged(SurfaceHolder holder,int format,int width,int height){
//            draw();
//        }
//
//        @Override
//        public void onSurfaceDestroyed(SurfaceHolder holder){
//            super.onSurfaceDestroyed(holder);
//            mVisible = false;
//            mHandler.removeCallbacks(mUpdateDisplay);
//        }
//
//        @Override
//        public void onDestroy(){
//            super.onDestroy();
//            mVisible = false;
//            mHandler.removeCallbacks(mUpdateDisplay);
//        }
//
//        //		------
//
//        static final String logTag = "backpaper2-ActivitySwipeDetector";
//        private static final int MIN_DISTANCE_LONG = 200;
//        private static final int MIN_DISTANCE_SHORT = 25;
//        private float downX, downY, upX, upY;
//
//        DisplayMetrics dm = getResources().getDisplayMetrics();
//        int REL_SWIPE_MIN_DISTANCE_LONG = (int)(MIN_DISTANCE_LONG * dm.densityDpi / 160.0f);
//        int REL_SWIPE_MIN_DISTANCE_SHORT = (int)(MIN_DISTANCE_SHORT * dm.densityDpi / 160.0f);
//
//        private int MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;
////		---------
//
//
//        @Override
//        public void setTouchEventsEnabled(boolean enabled) {
//            super.setTouchEventsEnabled(enabled);
//        }
//
//        @Override
//        public void onTouchEvent(MotionEvent event){
//            Log.d("backpaper2-wallpaper-server-touch","x: "+event.getX()+" y: "+event.getY());
//            switch(event.getAction()){
//                case MotionEvent.ACTION_DOWN: {
//                    downX = event.getX();
//                    downY = event.getY();
//                    return;
//                }
//                case MotionEvent.ACTION_UP: {
//
//                    upX = event.getX();
//                    upY = event.getY();
//
//                    float deltaX = downX - upX;
//                    float deltaY = downY - upY;
//
//                    // swipe horizontal?
//                    if(Math.abs(deltaX) > MIN_DISTANCE){
//                        // left or right
//                        if(deltaX < 0) {
//                            onLeftToRightSwipe();
////	                	return;
//                        }
//                        if(deltaX > 0) {
//                            onRightToLeftSwipe();
////	                	return;
//                        }
//                    }
//                    else {
//                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
////	                    return; // We don't consume the event
//                    }
//
//                    // swipe vertical?
//                    if(onBottomTop){
//                        MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_LONG;
//                    }else{
//                        MIN_DISTANCE = REL_SWIPE_MIN_DISTANCE_SHORT;
//                    }
//                    if(Math.abs(deltaY) > MIN_DISTANCE){
//                        // top or down
//                        if(deltaY < 0) {
//                            onTopToBottomSwipe(deltaY);
//                            return;
//                        }
//                        if(deltaY > 0) {
//                            onBottomToTopSwipe(deltaY);
//                            return; }
//                    }
//                    else {
//                        Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
//                        return; // We don't consume the event
//                    }
//
//                    return ;
//                }
//
//            }
//        }
//
//        private void onLeftToRightSwipe() {
//            Log.d("backpaper2-lwp","on left to right");
////			mHandler.removeCallbacks(mUpdateDisplay);
////			draw();
//
//        }
//
//        private void onRightToLeftSwipe() {
//            Log.d("backpaper2-lwp","on right to left");
////			mHandler.removeCallbacks(mUpdateDisplay);
////			draw();
//        }
//
////		System.out.println(sharedPrefs.getBoolean("swipe_change", true));
////		System.out.println(sharedPrefs.getBoolean("secret_swipe", true));
////		System.out.println(Integer.parseInt(sharedPrefs.getString("updates_interval", "10"))*1000);
//
//        private static final int SWIPE_TOP_TO_BOTTOM = 01100010;
//        private static final int SWIPE_BOTTOM_TO_TOP = 01100001;
//        private Boolean onBottomTop = false;
//        private int swipeDirection = 0;
//        private void onBottomToTopSwipe(float deltaY) {
//            swipeDirection = SWIPE_BOTTOM_TO_TOP;
//            if(sharedPreferences.getBoolean("swipe_change", true)){
//                if(sharedPreferences.getBoolean("secret_swipe", true)){
//                    Log.d("backpaper2-lwp","on bottom to top");
//                    if(deltaY >= REL_SWIPE_MIN_DISTANCE_LONG){
//                        Log.d("backpaper2-lwp","on bottom to top LONG");
//                        mHandler.removeCallbacks(mUpdateDisplay);
//                        onBottomTop = true;
//                        draw();
//                        mHandler.removeCallbacks(mUpdateDisplay);
//                    }else{
//                        onBottomTop = false;
//                        draw();
//                    }
//                }else{
//                    onBottomTop = false;
//                    draw();
//                }
//            }
//        }
//
//        private void onTopToBottomSwipe(float deltaY) {
//            swipeDirection = SWIPE_TOP_TO_BOTTOM;
//            if(sharedPreferences.getBoolean("swipe_change", true)){
//                Log.d("backpaper2-lwp","on top to bottom");
//                onBottomTop = false;
//                draw();
//            }
//        }
//
//        private ArrayList<Integer> getListNumbers(){
//            Set<String> enabledLists = sharedPreferences.getStringSet(IntentVariablesConfig.imageLiveWallpaperEnabledListsVariable,new TreeSet<String>());
//            ArrayList<Integer> resultList = new ArrayList<Integer>();
//            for(String eachEnabledList:enabledLists){
//                resultList.add(Integer.valueOf(eachEnabledList));
//            }
//            resultList.add(-1);
//            return resultList;
//        }
//
//        private int getCounter(){
//            return sharedPreferences.getInt(IntentVariablesConfig.imageLiveWallpaperListSelectedVariable,0);
//        }
//
//        private void setCounter(int val){
//            SharedPreferences.Editor editor = sharedPreferences.edit();
//            editor.putInt(IntentVariablesConfig.imageLiveWallpaperListSelectedVariable,val);
//            editor.commit();
//        }
//
//        private void draw(){
//            SurfaceHolder holder = getSurfaceHolder();
//            Canvas c = null;
//            MyDataSource myDataSource = new MyDataSource();
//            // TODO add long swipe settings
//            // TODO move this entire get path thing to a new function if time
//            int direction = 0;
//            if(swipeDirection == SWIPE_BOTTOM_TO_TOP)
//                direction = -1;
//            else
//                direction = 1;
////            int direction = 1; // this will be -1 if swiped backwards which has to be changed
//            ImagePathModel imagePathModel = myDataSource.getImagePath(context, getListNumbers(), getCounter() + direction, direction);
//            setCounter(imagePathModel.getCurrentRowId());
//            while(!GetPathMimeType.getMimeType(imagePathModel.getImagePath()).contains("image")){
//                imagePathModel = myDataSource.getImagePath(context,getListNumbers(),getCounter()+direction,direction);
//                setCounter(imagePathModel.getCurrentRowId());
//            }
//            //Log.d(AllConfigs.LWP_DEBUG_TAG,"-------------counter is "+imagePathModel.getCurrentRowId()+" direction is "+direction);
//            if(imagePathModel.getNoData()){
//                try {
//                    c = holder.lockCanvas();
//                    if (c != null) {
//                        c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                        Paint paint = new Paint();
//                        paint.setColor(Color.WHITE); // Text Color
//                        paint.setStrokeWidth(12); // Text Size
//                        paint.setTextSize(48);
//                        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)); // Text Overlapping Pattern
//                        int text_left_pos = (int)(10 * dm.densityDpi / 160.0f);
//                        int text_top_pos = (int)(60 * dm.densityDpi / 160.0f);
//                        int text_top_pos_count = (int)(34 * dm.densityDpi / 160.0f);
//                        c.drawText("List is EMPTY", text_left_pos, text_top_pos, paint);
//                        c.drawText("please Click on ", text_left_pos, text_top_pos+text_top_pos_count, paint);
//                        c.drawText("\"Add to LWP\" ", text_left_pos, text_top_pos+text_top_pos_count*2, paint);
//                        c.drawText("Button on an image", text_left_pos, text_top_pos+text_top_pos_count*3, paint);
//                        c.drawText("to add them", text_left_pos, text_top_pos+text_top_pos_count*4, paint);
//                    }
//                }catch (Exception e){
//                    Log.d(AllConfigs.LWP_DEBUG_TAG,"error at lwp "+e.toString());
//                }
//                finally {
//                    if (c != null)
//                        holder.unlockCanvasAndPost(c);
//                }
//                int timeOut = 10000;
//                mHandler.postDelayed(mUpdateDisplay, timeOut);
//                return;
//            }
//            try{
//                c = holder.lockCanvas();
//                if(c!=null){
//                    int mDstHeight = c.getHeight();
//                    int mDstWidth = c.getWidth();
//                    c.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
//                    File f = new File(imagePathModel.getImagePath());
//                    if(f.exists()){
//                        BitmapFactory.Options options = new BitmapFactory.Options();
//                        Bitmap unscaledBitmap = ScalingUtilities.decodeResourceFromImageFile(imagePathModel.getImagePath(),mDstWidth,mDstHeight, ScalingUtilities.ScalingLogic.CROP);
//                        Bitmap b = ScalingUtilities.createScaledBitmap(unscaledBitmap,mDstWidth,mDstHeight, ScalingUtilities.ScalingLogic.CROP);
//                        unscaledBitmap.recycle();
//                        c.drawBitmap(b,0,0,null);
//                    }
//                }
//            }catch (Exception e){
//                Log.d(AllConfigs.LWP_DEBUG_TAG,"Error while drawing on canvas "+e.toString());
//            }
//            finally {
//                if(c!=null)
//                    try{
//                        holder.unlockCanvasAndPost(c);
//                    }catch (Exception e){
//
//                    }
//                if(mVisible){
//                    mHandler.removeCallbacks(mUpdateDisplay);
//                    mHandler.postDelayed(mUpdateDisplay,3000);
//                }
//            }
//        }
//    }
//}
